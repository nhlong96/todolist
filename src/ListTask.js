import React, { Component } from 'react'
class ListTask extends Component {
    handleDeleteTask = taskId => {
        this.props.deleteTask(taskId)
    }
    createTasks = item => {
        return (
            <li key={item.key}>
            <input
                name="selectTask"
                type="checkbox"
                value={item.key}
                onChange={this.handleSelectTask}
                />
            {item.task}
            <button className="deleteTask" onClick={() => this.handleDeleteTask(item.key)}>X</button>
            </li>
        )
        }

    handleSelectTask = (e) => {
        this.props.selectTask(e.target.value, e.target.checked)        
    }
    render() {
    const { entries } = this.props;
    const listItems = entries.map(this.createTasks)

    return <ul className="theList">{listItems}</ul>
    }
}
export default ListTask