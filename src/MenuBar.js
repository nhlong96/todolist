import React, { Component } from 'react'
class MenuBar extends Component {
    changeTodoChanel = (e) => {
        e.preventDefault();
        this.props.changeChanel("todo")
    }
    changeCompleteChanel = (e) => {
        e.preventDefault();
        this.props.changeChanel("comlete")
    }
    render() {
        return (
            <div className="menuBar">
                <ul>
                    <a href="##" onClick={this.changeTodoChanel}>
                        <li>Home</li>
                    </a>
                    <a href="##" onClick={this.changeCompleteChanel}>
                        <li>Complete</li>
                    </a>
                </ul>
            </div>
        )
    }
}
export default MenuBar