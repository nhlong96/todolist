import React, { Component } from 'react'
class MoveBtn extends Component {
    handleMove = () => {
        this.props.moveTask()
    }
    // Render
    render() {
        var btnName = this.props.chanel === 'todo' ? 'Complete' : 'Back Todo'
        return (
            <button onClick={this.handleMove}>{btnName}</button>
        )
    }
}
export default MoveBtn