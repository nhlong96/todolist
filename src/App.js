
import React, { Component } from 'react'
import AddTask from './AddTask'
import ListTask from './ListTask'
import MenuBar from './MenuBar'
import MoveBtn from './MoveBtn'
class App extends Component {
  constructor() {
    super();
    this.state = {
      todo: [
          {
              key: 1,
              task: "Task1"
          },
          {
              key: 2,
              task: "Task2"
          },
          {
              key: 3,
              task: "Task3"
          }
      ],
      complete: [
        {
          key: 4,
          task: "Setup MySql"
        }
      ],
      select: [],
      chanel: "todo"
    };
  }
  addTask = (taskName) => {
    const newTask = {
      key: Date.now(),
      task: taskName
    }
    if (taskName !== null && taskName !== '') {
        this.setState(state =>  {
          return {
            todo: [...state.todo, newTask]
          }
      })
    }
  }
  selectTask = (taskId, checked) => {
    if(checked) {
      this.setState(state =>{
        return {
          select: [...state.select, taskId]
        }
      })
    } else {
      var newSelectList = this.state.select.filter(id => id !== taskId)
      this.setState(state => {
        return {
          select: newSelectList
        }
      })
    }
  }
  moveTask = () => {
    const select = this.state.select
    if(this.state.chanel === "todo") {
      let newCompleList = this.state.todo.filter(
        function(task) {
          return(
            select.indexOf(task.key.toString()) >= 0
          )
        }
      )
      const newTodoList = this.state.todo.filter(
        function(task) {
          return(
            select.indexOf(task.key.toString()) < 0
          )
        }
      )
      this.setState(state => {
        newCompleList = [...state.complete, ...newCompleList]
        return{
          todo: newTodoList,
          complete: newCompleList
        }
      })
    } else {
      let newTodoList = this.state.complete.filter(
        function(task) {
          return(
            select.indexOf(task.key.toString()) >= 0
          )
        }
      )
      let newCompleList = this.state.complete.filter(
        function(task) {
          return(
            select.indexOf(task.key.toString()) < 0
          )
        }
      )
      this.setState(state => {
        newTodoList = [...state.todo, ...newTodoList]
        return{
          todo: newTodoList,
          complete: newCompleList
        }
      })
    }
    
  }
  changeChanel = (cn) => {
    this.setState(state => {
      return{
        select: [],
        chanel: cn
      }
    })
  }
  deleteTask = (taskId) => {
    if (this.state.chanel === "todo") {
      let newTodoList = this.state.todo.filter(
        function(task) {
          return(
            task.key !== taskId
          )
        }
      )
      this.setState(state => {
        return{
          todo: newTodoList
        }
      })
    } else {
      let newCompleList = this.state.complete.filter(
        function(task) {
          return(
            task.key !== taskId
          )
        }
      )
      this.setState(state => {
        return{
          complete: newCompleList
        }
      })
    }
  }
  render() {
    let listTask
    if (this.state.chanel === "todo") {
      listTask = this.state.todo
    } else {
      listTask = this.state.complete
    }
    return (
        <div>
          <MenuBar changeChanel={this.changeChanel}/>
          <AddTask addTask={this.addTask} />
          <ListTask entries={listTask} selectTask={this.selectTask} deleteTask={this.deleteTask}/>
          <MoveBtn moveTask={this.moveTask} chanel={this.state.chanel}/>
        </div>
    )
  }
}
export default App