import React, { Component } from 'react'
class AddTask extends Component {
    handleAddTask = e => {
        e.preventDefault()
        const taskName = this.refs.taskInput.value;
        this.props.addTask(taskName);

        this.refs.taskInput.value = ''
        this.refs.taskInput.focus()

    }
    render() {
        return (
            <div className="todoListMain">
                <div className="header">
                    <form onSubmit={this.handleAddTask}>
                        <input 
                            placeholder="Task" 
                            ref="taskInput"
                            />
                        <button type="submit">Add</button>
                    </form>
                </div>
            </div>
        )
    }
}
export default AddTask